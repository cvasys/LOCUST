from pyvad import vad
from librosa import load
import sys # 3 arguments: name of audio file, frame_length, vad aggressiveness

filename = sys.argv[1] 
frame_length = int(sys.argv[2])  # accepts only 10, 20, 30
vad_mode = int(sys.argv[3])  # aggressiveness about filtering out non-speech fragments (1, 2, 3)

data, fs = load(filename)
result = vad(data, fs, fs_vad = 16000, hoplength = frame_length, vad_mode=vad_mode)

# print(result)