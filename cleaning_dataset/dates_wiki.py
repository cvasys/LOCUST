# This script looks for speakers' names and finds them in wikipedia
# As there are a lot of people with similar names, this the results of script working need to be checked manually
import json as JSON
import datetime
import requests

file_read = open("PATH-TO-FULL-JSON", 'r') # it is situated in https://drive.google.com/drive/u/1/folders/12JQ5KlnAfIRBlHvOFAZgUiXSF67rrEAF
file_content = ""
for line in file_read:
    file_content += line
file_json = JSON.loads(file_content)

file_birth = open("PATH-TO-FILE", 'r') # dates.txt
for line in file_birth:
    line = line.replace('\n', "")
    key = line.split('\t')[0]
    base_url = "https://en.wikipedia.org/wiki/"
    if "Unknown" in line:
        query = ""
        keys = key.split("_")
        for k in keys:
            k = k.title()
            query += k
            query += "_"
        page = requests.get(base_url + query[0:len(query) - 1])
        if "404" in str(page):
            pass
        else:
            print(base_url + query[0:len(query) - 1])
        if len(keys) == 3:
            query1 = keys[0].title() + "_" + keys[2].title()
            page1 = requests.get(base_url + query1)
            if "404" in str(page1):
                pass
            else:
                print(base_url + query1)
            query2 = keys[0].title() + "_" + keys[1].title() + "._" + keys[2].title()
            page2 = requests.get(base_url + query2)
            if "404" in str(page2):
                pass
            else:
                print(base_url + query2)