# Check if date of audio case was neither earlier than speaker's birth nor later than speaker's death

import json as JSON
from datetime import datetime
import os

date_format = '%B %d, %Y'

path = "PATH-TO-UNITED-JSON-FILE"
file_content = ""
file_read = open(path, 'r')
for line in file_read:
    file_content += line
file_json = JSON.loads(file_content)

prev_case = "0"

file_data = open("PATH-TO-FILE-WITH-DATES") # dates.txt
for line in file_data:
	line = line.replace('\n', "")
	info = line.split('\t')
	speaker_id = info[0]
	birth = info[1]
	death = info[2]
	for key in file_json:
		id = str(file_json[key]['info'][0]['ID'])
		if id == speaker_id:
			if "Unknown" not in birth:
				info_birth = birth.split("-")
				if len(info_birth) == 3:
					b = datetime.strptime(birth, "%Y-%m-%d").date()
				elif len(info_birth) == 2:
					b = datetime.strptime(birth, "%Y-%m").date()
				elif len(info_birth) == 1:
					b = datetime.strptime(birth, "%Y").date()
			if "Unknown" not in death and "alive" not in death:
				info_death = death.split("-")
				if len(info_death) == 3:
					d = datetime.strptime(death, "%Y-%m-%d").date()
				elif len(info_death) == 2:
					d = datetime.strptime(death, "%Y-%m").date()
				elif len(info_death) == 1:
					d = datetime.strptime(death, "%Y").date()
			audio_files = os.listdir("PATH-TO-FOLDER-WITH-AUDIO")
			for i in range(len(file_json[key]['transcripts'])):
				case_id = str(file_json[key]['transcripts'][i]['id'])
				for audio_file in audio_files:
					if case_id in audio_file:
						title = file_json[key]['transcripts'][i]['title']
						title_parts = title.split(" ")
						if ")" in title_parts[len(title_parts) - 1]:
							if "(Part" in title:
								if "audio" in title_parts[len(title_parts) - 1]:
									date_string = title_parts[len(title_parts) - 8] + " " + title_parts[len(title_parts) - 7] + " " + title_parts[len(title_parts) - 6]
								else:
									date_string = title_parts[len(title_parts) - 5] + " " + title_parts[len(title_parts) - 4] + " " + title_parts[len(title_parts) - 3]
							else:
								date_string = title_parts[len(title_parts) - 4] + " " + title_parts[len(title_parts) - 3] + " " + title_parts[len(title_parts) - 2]
						else:
							date_string = title_parts[len(title_parts) - 3] + " " + title_parts[len(title_parts) - 2] + " " + title_parts[len(title_parts) - 1]
						case_date = datetime.strptime(date_string, date_format).date()
						if "Unknown" not in birth:
							if case_date <= b:
								print("BEFORE BIRTH", speaker_id + "/" + audio_file[:audio_file.find('_')])
						if "Unknown" not in death and "alive" not in death:
							if case_date >= d:
								print("AFTER DEATH", speaker_id + "/" + audio_file[:audio_file.find('_')])
						if "Unknown" not in birth and ("Unknown" in death or "alive" in death):
                            diff = str(case_date - b)
                            age = int(diff[:diff.find(' ')]) / 365
                            if age >= 100:
                                print("MORE THAN 100 YEARS", speaker_id + "/" + audio_file[:audio_file.find('_')])
						break



