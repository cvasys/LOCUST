# UNITING ALL THE INFORMATION FROM https://github.com/walkerdb/supreme_court_transcripts INTO ONE JSON
import os
import json as JSON
import urllib.request
import codecs

# Collect personal information about the speaker
def get_info(all_data, identifier, file_json):
    info = {}
    for a in range(len(file_json['transcript']['sections'])):
        for b in range(len(file_json['transcript']['sections'][a]['turns'])):
            try:
                if(identifier == file_json['transcript']['sections'][a]['turns'][b]['speaker']['identifier']):
                    info['ID'] = file_json['transcript']['sections'][a]['turns'][b]['speaker']['ID']
                    info['name'] = file_json['transcript']['sections'][a]['turns'][b]['speaker']['name']
                    href = file_json['transcript']['sections'][a]['turns'][b]['speaker']['href']
                    with urllib.request.urlopen(href) as url:
                        info_json = JSON.loads(url.read().decode())
                    info['first_name'] = info_json['first_name']
                    info['middle_name'] = info_json['middle_name']
                    info['last_name'] = info_json['last_name']
                    info['name_suffix'] = info_json['name_suffix']
                    info['date_of_birth'] = info_json['date_of_birth']
                    info['place_of_birth'] = info_json['place_of_birth']
                    info['date_of_death'] = info_json['date_of_death']
                    info['place_of_death'] = info_json['place_of_death']
                    info['gender'] = info_json['gender']
                    info['ethnicity'] = info_json['ethnicity']
                    info['biography'] = info_json['biography']
                    for c in range(len(info_json['roles'])):
                        roles = {}
                        roles['date_start'] = info_json['roles'][c]['date_start']
                        roles['date_end'] = info_json['roles'][c]['date_end']
                        roles['appointing_president'] = info_json['roles'][c]['appointing_president']
                        roles['role_title'] = info_json['roles'][c]['role_title']
                        roles['institution_name'] = info_json['roles'][c]['institution_name']
                        info.setdefault('roles', []).append(roles)
                    info['size'] = info_json['size']
                    info['religion'] = info_json['religion']
                    info['length_of_service'] = info_json['length_of_service']
                    info['law_school'] = info_json['law_school']
                    info['home_state'] = info_json['home_state']
            except:
                pass
    all_data[identifier].setdefault('info', []).append(info)
    return all_data

# Collect information about transcripts of the speaker
def get_transcripts(all_data, identifier, file_json):
    transcript = {}
    transcript['id'] = file_json['id']
    transcript['title'] = file_json['title']
    transcript['media_file'] = file_json['media_file']
    for a in range(len(file_json['transcript']['sections'])):
        for b in range(len(file_json['transcript']['sections'][a]['turns'])):
            try:
                if(identifier == file_json['transcript']['sections'][a]['turns'][b]['speaker']['identifier']):
                    for c in range(len(file_json['transcript']['sections'][a]['turns'][b]['text_blocks'])):
                        text_block = {}
                        text_block['start'] = file_json['transcript']['sections'][a]['turns'][b]['text_blocks'][c]['start']
                        text_block['stop'] = file_json['transcript']['sections'][a]['turns'][b]['text_blocks'][c]['stop']
                        text_block['byte_start'] = file_json['transcript']['sections'][a]['turns'][b]['text_blocks'][c]['byte_start']
                        text_block['byte_stop'] = file_json['transcript']['sections'][a]['turns'][b]['text_blocks'][c]['byte_stop']
                        text_block['text'] = file_json['transcript']['sections'][a]['turns'][b]['text_blocks'][c]['text']
                        transcript.setdefault('text_blocks', []).append(text_block)
                else:
                    pass
            except:
                pass
    all_data[identifier].setdefault('transcripts', []).append(transcript)
    return all_data

# Forming united json
path = "PATH-TO-FILES-FROM-GITHUB"
all_data = {}
files = os.listdir(path)
for file in files:
    if "-t" in file:
        file_read = codecs.open(path + file, 'r', 'utf_8_sig')
        file_content = ""
        for line in file_read:
            file_content += line
        file_json = JSON.loads(file_content)
        identifiers = []
        try:
            for i in range(len(file_json['transcript']['sections'])):
                for j in range(len(file_json['transcript']['sections'][i]['turns'])):
                    try:
                        identifiers.append(file_json['transcript']['sections'][i]['turns'][j]['speaker']['identifier'])
                    except:
                        pass
            identifiers = list(set(identifiers))
            for t in range(len(identifiers)):
                if identifiers[t] in all_data:
                    all_data = get_transcripts(all_data, identifiers[t], file_json)
                else:
                    all_data[identifiers[t]] = {}
                    all_data = get_transcripts(all_data, identifiers[t], file_json)
                    all_data = get_info(all_data, identifiers[t], file_json)
            print(file)
        except:
            print("NO TRANSCRIPTS IN", file)

# Writing united json to file
with open("PATH-TO-FILE", 'w') as outfile:
    JSON.dump(all_data, outfile)