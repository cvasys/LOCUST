# PRODUCING STATISTICS ON SPEAKERS
import json as JSON
from datetime import datetime

# Getting information about speakers
path = "PATH-TO-FILE-WITH-UNITED-JSON"
file_content = ""
file_read = open(path, 'r')
for line in file_read:
	file_content += line
file_json = JSON.loads(file_content)

date_format = '%B %d, %Y'

# Writng tatistics on speakers to file
path_2 = "PATH-TO-FILE-WITH-STATISTICS-ON-SPEAKERS"
file_statistics_on_speakers = open(path_2, 'w')
for key in file_json:
	dates = []
	speaker_id = file_json[key]['info'][0]['ID']
	speaker_identifier = key
	speaker_name = file_json[key]['info'][0]['name']
	speaker_appearances = str(len(file_json[key]['transcripts']))
	sum = 0
	for i in range(len(file_json[key]['transcripts'])):
		for j in range(len(file_json[key]['transcripts'][i]['text_blocks'])):
			time = file_json[key]['transcripts'][i]['text_blocks'][j]['stop'] - file_json[key]['transcripts'][i]['text_blocks'][j]['start']
			sum += time
		case_id = str(file_json[key]['transcripts'][i]['id'])
		title = file_json[key]['transcripts'][i]['title']
		title_parts = title.split(" ")
		if ")" in title_parts[len(title_parts) - 1]:
			if "(Part" in title:
				if "audio" in title_parts[len(title_parts) - 1]:
					date_string = title_parts[len(title_parts) - 8] + " " + title_parts[len(title_parts) - 7] + " " + title_parts[len(title_parts) - 6]
				else:
					date_string = title_parts[len(title_parts) - 5] + " " + title_parts[len(title_parts) - 4] + " " + title_parts[len(title_parts) - 3]
			else:
				date_string = title_parts[len(title_parts) - 4] + " " + title_parts[len(title_parts) - 3] + " " + title_parts[len(title_parts) - 2]
		else:
			date_string = title_parts[len(title_parts) - 3] + " " + title_parts[len(title_parts) - 2] + " " + title_parts[len(title_parts) - 1]
		date = datetime.strptime(date_string, date_format).date()
		dates.append(date)
	total_speaking_time = str(sum)
	min_date = dates[0]
	for t in range(len(dates)):
		if (dates[t] < min_date):
			min_date = dates[t]
	max_date = dates[0]
	for s in range(len(dates)):
		if(dates[s] > max_date):
			max_date = dates[s]
	
	earliest_appearance_date = min_date
	latest_appearance_date = max_date
	
	file_statistics_on_speakers.write(speaker_id + '\t' + speaker_identifier + '\t' + speaker_name + '\t' + speaker_appearances + '\t' + total_speaking_time + '\t' + earliest_appearance_date + '\t' + latest_appearance_date + '\n')
