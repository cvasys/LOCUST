# SEPARATING AUDIO ACCORDING TO TIMESTAMPS OF SPEAKERS
import json as JSON
import os

from pydub import AudioSegment


def convert_to_mp3(source):
    destination = source.replace(".wav", ".mp3")
    os.system("ffmpeg -loglevel error -i {} -vn -ar 16000 -ac 1 -ab 160 -f mp3 {}".format(source, destination))

path = "PATH-TO-FILE-WITH-UNITED-JSON"
file_content = ""
file_read = open(path, 'r')
for line in file_read:
    file_content += line
file_json = JSON.loads(file_content)

for key in file_json:
    speaker_id = str(file_json[key]['info'][0]['ID'])
    os.makedirs("PATH-TO-SEPARATED-AUDIO" + speaker_id)
    print(key)
    for i in range(len(file_json[key]['transcripts'])):
        case_id = str(file_json[key]['transcripts'][i]['id'])
        audio_name = AudioSegment.from_mp3("PATH-TO-FOLDER-WITH-FULL-AUDIO" + case_id + ".mp3")
        for j in range(len(file_json[key]['transcripts'][i]['text_blocks'])):
            start = file_json[key]['transcripts'][i]['text_blocks'][j]['start'] * 1000
            end = file_json[key]['transcripts'][i]['text_blocks'][j]['stop'] * 1000 + 1000
            result_audio = audio_name[start : end]
            result_audio_name = "PATH-TO-FOLDER-WITH-SEPARATED-AUDIO" + speaker_id + "/" + case_id + "_" + str(j) + ".wav"
            try:
                result_audio.export(result_audio_name, format = "wav")
                # temporary solution. Should be replaced with AudioSegment.export
                convert_to_mp3(result_audio_name)
            except:
                print("WARNING", "speaker", key, "transcript", str(i), "text_block", str(j))