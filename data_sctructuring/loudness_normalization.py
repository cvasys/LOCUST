# DOING LOUDNESS NORMALIZATION
import os
from pydub import AudioSegment

def match_target_amplitude(sound, target_dBFS):
        change_in_dBFS = target_dBFS - sound.dBFS
        return sound.apply_gain(change_in_dBFS)

sound = AudioSegment.from_file("PATH-TO-SOURCE-AUDIO-FILE")
normalized_sound = match_target_amplitude(sound, -20.0)
normalized_sound.export("PATH-TO-OUTPUT-AUDIO-FILE", format = "wav")
