# DOWNLOADING SOURCE MP3 FILES
import json as JSON
import urllib.request, json 
import os
from urllib.request import urlretrieve 
import codecs

path = "PATH-TO-FILES-FROM-GITHUB"
files = os.listdir(path)
for file in files:
    if "-t" in file:
        file_read = codecs.open(path + file, 'r', 'utf_8_sig')
        file_content = ""
        for line in file_read:
            file_content += line
        file_json = JSON.loads(file_content)
        if(file_json['transcript'] == None):
            Id = str(file_json['id'])
            if(file_json['media_file'] != None):
                for i in range(len(file_json['media_file'])):
                    if(file_json['media_file'][i]['mime'] == "audio/mpeg"):
                        url = file_json['media_file'][i]['href']
                        destination = "PATH-TO-FOLDER-FOR-AUDIO-WITHOUT-TRANSCRIPTS" + Id + ".mp3"
                        try:
                            urlretrieve(url, destination)
                        except:
                            print("WARNING")
            else:
                print("NOMEDIA")
        else:
            Id = str(file_json['id'])
            if(file_json['media_file'] != None):
                for i in range(len(file_json['media_file'])):
                    if(file_json['media_file'][i]['mime'] == "audio/mpeg"):
                        url = file_json['media_file'][i]['href']
                        destination = "PATH-TO-FOLDER-FOR-AUDIO-WITH-TRANSCRIPTS" + Id + ".mp3"
                        try:
                            urlretrieve(url, destination)
                        except:
                            print("WARNING")
            else:
                print("NOMEDIA")