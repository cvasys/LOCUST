#!/bin/bash

#
# script is designed to convert .wav files to .mp3
#
# input parameters:
# 	* $1 - path to the directory from which to start searching for all nested files with the extension .wav
#	* $2 - path to the directory where the converted files will be saved
#
# default parameters:
#   * $1 - "/data/parsed_audio"
#   * $2 - "/data/converted_parsed_audio"

source_folder_path=$1
if ! [ -n "$1" ]; then source_folder_path="/data/parsed_audio"; fi

result_folder_path=$2
if ! [ -n "$2" ]; then result_folder_path="/data/converted_parsed_audio"; fi

for file in `find ${source_folder_path} -type f -name "*.wav"`
do
	filename=$(basename ${file})
	filename="${filename%.*}"
	sub_dir=$(dirname ${file#*${source_folder_path}})

	result_dir_path=${result_folder_path}${sub_dir}
	if ! [ -d ${result_dir_path} ]
	then
		echo 'create directory '${result_dir_path}
		mkdir -p ${result_dir_path}
	fi
	result_path=${result_dir_path}'/'${filename}'.mp3'
	echo ${file} ' -> ' ${result_path}

	ffmpeg -loglevel error -i ${file} -vn -ar 16000 -ac 1 -ab 160 -f mp3 ${result_path}
done