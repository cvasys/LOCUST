import argparse
import glob
import os
import logging

import librosa
import sys
import numpy as np
from sidekit.frontend.features import pre_emphasis, dct

PARAM_TYPE = np.float32


def hz2mel(f):
    # Convert an array of frequency in Hz into mel.
    return 2595 * np.log10(1 + f / 700.)


def mel2hz(z):
    # onvert an array of mel values in Hz.
    return 700. * (10 ** (z / 2595.) - 1)


def trfbank(fs, nfft, lowfreq, maxfreq, nlinfilt, nlogfilt):
    # Compute triangular filterbank for cepstral coefficient computation.
    nfilt = nlinfilt + nlogfilt
    low_mel = hz2mel(lowfreq)
    max_mel = hz2mel(maxfreq)
    mels = np.zeros(nlogfilt + 2)
    melsc = (max_mel - low_mel) / (nfilt + 1)
    mels[:nlogfilt + 2] = low_mel + np.arange(nlogfilt + 2) * melsc
    frequencies = mel2hz(mels)
    heights = 2. / (frequencies[2:] - frequencies[0:-2])
    fbank = np.zeros((nfilt, int(np.floor(nfft / 2)) + 1), dtype=PARAM_TYPE)
    n_frequencies = np.arange(nfft) / (1. * nfft) * fs
    for i in range(nfilt):
        low = frequencies[i]
        cen = frequencies[i + 1]
        hi = frequencies[i + 2]
        lid = np.arange(np.floor(low * nfft / fs) + 1, np.floor(cen * nfft / fs) + 1, dtype=np.int)
        left_slope = heights[i] / (cen - low)
        rid = np.arange(np.floor(cen * nfft / fs) + 1, min(np.floor(hi * nfft / fs) + 1, nfft), dtype=np.int)
        right_slope = heights[i] / (hi - cen)
        fbank[i][lid] = left_slope * (n_frequencies[lid] - low)
        fbank[i][rid[:-1]] = right_slope * (hi - n_frequencies[rid[:-1]])
    return fbank, frequencies


def framing(sig, window_length, window_shift, context=(0, 0)):
    # Separate input signal into frames of determined length and overlap
    dsize = sig.dtype.itemsize
    if sig.ndim == 1:
        sig = sig[:, np.newaxis]
    c = (context,) + (sig.ndim - 1) * ((0, 0),)
    _win_size = window_length + sum(context)
    shape = (int((sig.shape[0] - window_length) / window_shift) + 1, 1, _win_size, sig.shape[1])
    strides = tuple(map(lambda x: x * dsize, [window_shift * sig.shape[1], 1, sig.shape[1], 1]))
    return np.lib.stride_tricks.as_strided(np.lib.pad(sig, c, 'constant', constant_values=(0,)), shape=shape,
                                           strides=strides).squeeze()


def power_spectrum(sig, fs=16000, nwin=0.025, shift=0.01, prefac=0.97):
    # Compute the power spectrum of the signal
    window_length = int(round(nwin * fs))
    overlap = window_length - int(shift * fs)
    window_shift = window_length - overlap
    framed = framing(sig, window_length, window_shift).copy()
    framed = pre_emphasis(framed, prefac)
    l = framed.shape[0]
    n_fft = 2 ** int(np.ceil(np.log2(window_length)))
    window = np.hamming(window_length)
    spec = np.ones((l, int(n_fft / 2) + 1), dtype=PARAM_TYPE)
    log_energy = np.log((framed ** 2).sum(axis=1))
    dec = 500000
    start = 0
    stop = min(dec, l)
    while start < l:
        ahan = framed[start:stop, :] * window
        mag = np.fft.rfft(ahan, n_fft, axis=-1)
        spec[start:stop, :] = mag.real ** 2 + mag.imag ** 2
        start = stop
        stop = min(stop + dec, l)
    return spec, log_energy


def mfcc(sig, lowfreq=100, maxfreq=8000, fs=16000, nlinfilt=0, nlogfilt=24,
         nwin=0.025, nceps=13, shift=0.01, prefac=0.97, need_energy=False):
    spec, log_energy = power_spectrum(sig, fs, nwin, shift, prefac)
    n_fft = 2 ** int(np.ceil(np.log2(int(round(nwin * fs)))))
    fbank = trfbank(fs, n_fft, lowfreq, maxfreq, nlinfilt, nlogfilt)[0]
    mspec = np.log(np.dot(spec, fbank.T))
    # To fix NaNs it was suggested adding epsilon to the elements of spectrum matrix
    ceps = dct(mspec + sys.float_info.epsilon, type=2, norm='ortho', axis=-1)[:, 1:nceps + 1]
    if need_energy:
        return np.concatenate((ceps, log_energy.reshape(log_energy.shape + (1,))), axis=1)
    return ceps


if __name__ == "__main__":
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("-i", "--input",  type=str, required=True, help="path to files to calculate mfcc")
    parser.add_argument("-o", "--output", type=str, required=True, help="root of output files (subpath of input)")
    parser.add_argument("-E", "--log_energy", action="store_true", help="calculate log energy")
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO, format='%(asctime)-15s %(message)s')
    logger = logging.getLogger()

    result_root_dir = os.path.join(args.output, "mfcc_feature")

    for path in glob.glob(args.input, recursive=True):
        absolute_path = os.path.abspath(path)
        if not absolute_path.startswith(args.output):
            raise ValueError("output must be subpath of input")

        file_name = os.path.splitext(os.path.basename(path))[0] + ".csv"
        dir_name = os.path.join(result_root_dir, os.path.dirname(path)[len(args.output):])
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        try:
            sig, fs = librosa.load(path, sr=16000)
            data = mfcc(sig, fs=fs, need_energy=args.log_energy)
        except Exception:
            logger.error("data processing error. file %s", path)
            continue

        # filter lines that contain only NaN values
        # presumably these frames do not contain audio information
        data = data[np.array([not all(map(np.isnan, line)) for line in data])]

        result_path = os.path.join(dir_name, file_name)
        logger.info('mfcc features for the file "%s" saved in "%s"', path, result_path)
        np.savetxt(result_path, data, delimiter=";")

