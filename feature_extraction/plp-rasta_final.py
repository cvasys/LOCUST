import math
import librosa
import numpy.matlib
import sys # 3 arguments: path to json file, path to folder with audio, path to folder with results
import numpy as np
import scipy
import scipy.io.wavfile as wav
from scipy.fftpack.realtransforms import dct
from sidekit.frontend.vad import pre_emphasis
from sidekit.frontend.io import *
from sidekit.frontend.normfeat import *
from sidekit.frontend.features import *

PARAM_TYPE = numpy.float32

def bark2hz(z):
	# Convert frequencies Bark to Hertz (Hz)
	return 600. * np.sinh(z / 6.)

def hz2bark(f):
	# Convert frequencies (Hertz) to Bark frequencies
	return 6. * np.arcsinh(f / 600.)

def fft2barkmx(n_fft, fs, nfilts=0, width=1., minfreq=133.33, maxfreq=8000):
	# Generate a matrix of weights to combine FFT bins into Bark bin
	min_bark = hz2bark(minfreq)
	nyqbark = hz2bark(maxfreq) - min_bark
	if (nfilts == 0):
		nfilts = np.ceil(nyqbark) + 1
	nfilts = int(nfilts)
	wts = np.zeros((nfilts, n_fft))
	step_barks = nyqbark / (nfilts - 1)
	binbarks = hz2bark(np.arange(n_fft / 2 + 1) * fs / n_fft)
	for i in range(nfilts):
		f_bark_mid = min_bark + i * step_barks
		lof = (binbarks - f_bark_mid - 0.5)
		hif = (binbarks - f_bark_mid + 0.5)
		wts[i, :n_fft // 2 + 1] = 10 ** (np.minimum(np.zeros_like(hif), np.minimum(hif, -2.5 * lof) / width))
	return wts

def framing(sig, window_length, window_shift, context = (0, 0), pad = 'zeros'):
	# Separate input signal into frames of determined length and overlap
	dsize = sig.dtype.itemsize
	if sig.ndim == 1:
		sig = sig[:, np.newaxis]
	c = (context, ) + (sig.ndim - 1) * ((0, 0), )
	_win_size = window_length + sum(context)
	shape = (int((sig.shape[0] - window_length) / window_shift) + 1, 1, _win_size, sig.shape[1])
	strides = tuple(map(lambda x: x * dsize, [window_shift * sig.shape[1], 1, sig.shape[1], 1]))
	return np.lib.stride_tricks.as_strided(np.lib.pad(sig, c, 'constant', constant_values = (0,)), shape = shape, strides = strides).squeeze()

def power_spectrum(sig, fs = 16000, nwin = 0.025, shift = 0.01, prefac = 0.97):
	# Compute the power spectrum of the signal
	window_length = int(round(nwin * fs))
	overlap = window_length - int(shift * fs)
	window_shift = window_length - overlap
	framed = framing(sig, window_length, window_shift).copy()
	framed = pre_emphasis(framed, prefac)
	l = framed.shape[0]
	n_fft = 2 ** int(np.ceil(np.log2(window_length)))
	window = np.hamming(window_length)
	spec = np.ones((l, int(n_fft / 2) + 1), dtype = PARAM_TYPE)
	log_energy = np.log((framed ** 2).sum(axis = 1))
	dec = 500000
	start = 0
	stop = min(dec, l)
	while start < l:
		ahan = framed[start:stop, :] * window
		mag = np.fft.rfft(ahan, n_fft, axis=-1)
		spec[start:stop, :] = mag.real**2 + mag.imag**2
		start = stop
		stop = min(stop + dec, l)
	return spec, log_energy

def audspec(powspec, fs, maxfreq, nfilts = 0, fbtype = 'bark', minfreq = 133.33, sumpower = True, width = 1.):
	# Group powerspectrum to critical band according to bark-scale
	if(nfilts == None):
		nfilts = int(np.ceil(hz2bark(fs / 2)) + 1)
	if not fs == 16000:
		maxfreq = min(fs / 2, maxfreq)
	nframes, nfreqs = powspec.shape
	n_fft = (nfreqs -1 ) * 2
	wts = fft2barkmx(n_fft, fs, maxfreq, nfilts, width)
	wts = wts[:, :nfreqs]
	audio_spectrum = powspec.dot(wts.T)
	return audio_spectrum, wts

def postaud(x, fmax, fbtype = 'bark', broaden = 0):
	# Do loudness equalization and cube root compression
	nframes, nbands = x.shape
	nfpts = nbands + 2 * broaden
	bandcfhz = bark2hz(np.linspace(0, hz2bark(fmax), num = nfpts))
	bandcfhz = bandcfhz[broaden:(nfpts - broaden)]
	fsq = bandcfhz ** 2
	ftmp = fsq + 1.6e5
	eql = ((fsq / ftmp) ** 2) * ((fsq + 1.44e6) / (fsq + 9.61e6))
	z = np.matlib.repmat(eql.T,nframes,1) * x
	z = z ** .33
	y = z[:, np.hstack((1, np.arange(1, nbands - 1), nbands - 2))]
	return y, eql

def dolpc(x, model_order = 8):
	# Compute autoregressive model from spectral magnitude samples
	nframes, nbands = x.shape
	r = np.real(np.fft.ifft(np.hstack((x,x[:,np.arange(nbands-2,0,-1)]))))
	r = r[:, :nbands]
	y_lpc = np.ones((r.shape[0], model_order + 1))
	for ff in range(r.shape[0]):
		y_lpc[ff, 1:], e, _ = levinson(r[ff, :-1].T, order = model_order, allow_singularity = True)
		y_lpc[ff, :] /= e
	return y_lpc

def lpc2spec(lpcas, nout = 17):
	# Convert LPC coeffs back into spectra
	[cols, rows] = lpcas.shape
	order = rows - 1
	gg = lpcas[:, 0]
	aa = lpcas / np.tile(gg, (rows,1)).T
	zz = np.exp((-1j * np.pi / (nout - 1)) * np.outer(np.arange(nout).T,  np.arange(order + 1)))
	features = ( 1./np.abs(aa.dot(zz.T))**2) / np.tile(gg, (nout, 1)).T
	F = np.zeros((cols, rows-1))
	M = np.zeros((cols, rows-1))
	for c in range(cols):
		aaa = aa[c, :]
		rr = np.roots(aaa)
		ff = np.angle(rr.T)
		zz = np.exp(1j * np.outer(ff, np.arange(len(aaa))))
		mags = np.sqrt(((1./np.abs(zz.dot(aaa)))**2)/gg[c])
		ix = np.argsort(ff)
		keep = ff[ix] > 0
		ix = ix[keep]
		F[c, np.arange(len(ix))] = ff[ix]
		M[c, np.arange(len(ix))] = mags[ix]
	F = F[:, F.sum(axis=0) != 0]
	M = M[:, M.sum(axis=0) != 0]
	return features, F, M

def lpc2cep(a, nout):
	# Convert the LPC 'a' coefficients in each column of lpcas
	ncol, nin = a.shape
	order = nin - 1
	if nout is None:
		nout = order + 1
	c = np.zeros((ncol, nout))
	c[:, 0] = -np.log(a[:, 0])
	a /= np.tile(a[:, 0][:, None], (1, nin))
	for n in range(1, nout):
		sum = 0
		for m in range(1, n):
			sum += (n - m)  * a[:, m] * c[:, n - m]
		c[:, n] = -(a[:, n] + sum / n)
	return c

def spec2cep(spec, ncep=13, type=2):
	# Calculate cepstra from spectral samples
	nrow, ncol = spec.shape
	dctm = np.zeros((ncep, nrow))
	i = 1
	while(i < 14):
		dctm[i,:] = np.cos((i-1)*dctm[1:2:(2*nrow-1)]/(2*nrow)*np.pi) * np.sqrt(2/nrow)
		i += 1
	dctm[1,:] = dctm[1,:]/np.sqrt(2)
	cep = dctm*np.log(spec + sys.float_info.epsilon) # To fix NaNs it was suggested adding epsilon to the elements of spectrum matrix
	return cep

def lifter(x, lift=0.6, invs=False):
	# Apply lifter to matrix of cepstra, lift = exponent of x i^n liftering
	nfrm, ncep = x.shape
	if lift == 0:
		y = x
	else:
		if lift > 0:
			if lift > 10:
				print('Unlikely lift exponent of {} did you mean -ve?'.format(lift))
			liftwts = np.hstack((1, np.arange(1, ncep)**lift))
		elif lift < 0:
			L = float(-lift)
			if (L != np.round(L)):
				print('HTK liftering value {} must be integer'.format(L))
			liftwts = np.hstack((1, 1 + L/2*np.sin(np.arange(1, ncep) * np.pi / L)))
		y = x.dot(np.diag(liftwts))
	return y

def levinson(r, order = None, allow_singularity = False):
	# Levinson-Durbin recursion
	T0  = np.real(r[0])
	T = r[1:]
	M = len(T)
	if order is None:
		M = len(T)
	else:
		M = order
	realdata = np.isrealobj(r)
	if realdata is True:
		A = np.zeros(M, dtype = float)
		ref = np.zeros(M, dtype = float)
	else:
		A = np.zeros(M, dtype = complex)
		ref = np.zeros(M, dtype = complex)
	P = T0
	for k in range(M):
		save = T[k]
		if k == 0:
			temp = -save / P
		else:
			for j in range(0, k):
				save = save + A[j] * T[k-j-1]
			temp = -save / P
		if realdata:
			P = P * (1. - temp ** 2.)
		else:
			P = P * (1. - (temp.real ** 2 + temp.imag ** 2))
		if (P <= 0).any() and allow_singularity==False:
			raise ValueError("singular matrix")
		A[k] = temp
		ref[k] = temp
		if k == 0:
			continue
		khalf = (k + 1) // 2
		if realdata is True:
			for j in range(0, khalf):
				kj = k-j-1
				save = A[j]
				A[j] = save + temp * A[kj]
				if j != kj:
					A[kj] += temp * save
		else:
			for j in range(0, khalf):
				kj = k-j-1
				save = A[j]
				A[j] = save + temp * A[kj].conjugate()
				if j != kj:
					A[kj] = A[kj] + temp * save.conjugate()
	return A, P, ref

def plp(sig, fs = 16000, nwin = 0.025, plp_order=13, shift=0.01, get_spec=False, get_mspec=False, prefac=0.97, rasta=True):
	# PLP-RASTA
	plp_order -= 1
	powspec, log_energy = power_spectrum(sig, fs, nwin, shift, prefac)
	maxfreq = 8000
	audio_spectrum = audspec(powspec, fs, maxfreq)[0]
	nbands = audio_spectrum.shape[0]
	nl_aspectrum = np.log(audio_spectrum)
	ras_nl_aspectrum = rasta_filt(nl_aspectrum)
	audio_spectrum = np.exp(ras_nl_aspectrum)
	post_spectrum = postaud(audio_spectrum, fs / 2.)[0]
	if plp_order > 0:
		lpcas = dolpc(post_spectrum, plp_order)
		cepstra = lpc2cep(lpcas, plp_order + 1)
	else:
		spectra = post_spectrum
		cepstra = spec2cep(spectra)
	cepstra = lifter(cepstra)
	lst = list()
	lst.append(cepstra)
	lst.append(log_energy)
	return lst
	
path = sys.argv[1] # Path to json file. In our case: "/data/old/scripts/all_data.json"
file_content = ""
file_read = open(path, 'r')
for line in file_read:
    file_content += line
file_json = JSON.loads(file_content)


for speaker in file_json:
	speaker_id = str(file_json[speaker]['info'][0]['ID'])
	name = speaker_id + ".txt"
	files = os.listdir(sys.argv[2] + "/" + speaker_id) # Path to folder with audio. In our case: "/data/data/parsed_audio"
	file_result = open(sys.argv[3] + "/" + name,'w') # Path to folder for results. Example: "/data/coefficients/plp-rasta"
	date_format = '%B %d, %Y'
	dates = {}
	for i in range(len(file_json[speaker]['transcripts'])):
		case_id = str(file_json[speaker]['transcripts'][i]['id'])
		title = file_json[speaker]['transcripts'][i]['title']
		title_parts = title.split(" ")
		if ")" in title_parts[len(title_parts) - 1]:
			if "(Part" in title:
				if "audio" in title_parts[len(title_parts) - 1]:
					date_string = title_parts[len(title_parts) - 8] + " " + title_parts[len(title_parts) - 7] + " " + title_parts[len(title_parts) - 6]
				else:
					date_string = title_parts[len(title_parts) - 5] + " " + title_parts[len(title_parts) - 4] + " " + title_parts[len(title_parts) - 3]
			else:
				date_string = title_parts[len(title_parts) - 4] + " " + title_parts[len(title_parts) - 3] + " " + title_parts[len(title_parts) - 2]
		else:
			date_string = title_parts[len(title_parts) - 3] + " " + title_parts[len(title_parts) - 2] + " " + title_parts[len(title_parts) - 1]
		date = datetime.strptime(date_string, date_format).date()
		dates[case_id] = date

	dates = sorted(dates.items(), key = operator.itemgetter(1))

	for key in dates:
		key = str(key).split(",")
		key = key[0][2:-1]
		for i in range(len(file_json[speaker]['transcripts'])):
			case_id = str(file_json[speaker]['transcripts'][i]['id'])
			if(case_id == key):
				for j in range(len(file_json[speaker]['transcripts'][i]['text_blocks'])):
					Id = case_id + "_" + str(j)
					file = case_id + "_" + str(j) + ".mp3"
					if file in files:
						sig, fs = librosa.load(sys.argv[2] + "/" + speaker_id + "/" + file, sr = 16000)
						try:
							lst = mfcc(sig, fs)
							file_result.write(Id + '\n')
							for t in range(len(lst[0])):
								file_result.write(str(lst[0][t][0]) + '\t' + str(lst[0][t][1]) + '\t' + str(lst[0][t][2]) + '\t' + str(lst[0][t][3]) + '\t' + str(lst[0][t][4]) + '\t' + str(lst[0][t][5]) + '\t' + str(lst[0][t][6]) + '\t' + str(lst[0][t][7]) + '\t' + str(lst[0][t][8]) + '\t' + str(lst[0][t][9]) + '\t' + str(lst[0][t][10]) + '\t' + str(lst[0][t][11]) + '\t' +str(lst[0][t][12]) + '\n')
						except:
							print(file)